Pod::Spec.new do |s|

  s.name            = "Extensionsss"
  s.version         = "1.5.2"
  s.summary         = "Extensions helpers for Yesss Apps."
  s.description     = "Extensions helpers to be used in Yesss Applications."
  s.homepage        = "https://bitbucket.org/ygmd/yesssapps.extensions.ios"
  s.license         = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.author          = { "Djamil Secco" => "ds@yesss.lu" }
  s.platform        = :ios, "11.4"
  s.source          = { :git => "https://djsec@bitbucket.org/ygmd/yesssapps.extensions.ios.git", :tag => "#{s.version}" }
  s.source_files    = "Extensionsss", "Extensionsss/Extensions"
  s.swift_version   = "5"

end
