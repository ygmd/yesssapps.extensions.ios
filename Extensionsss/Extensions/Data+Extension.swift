//
//  Data+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

public extension Data {
  mutating func appendString(string: String) {
    let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
    append(data!)
  }
  
  func jsonDictionary() -> [String: AnyObject]? {
    do {
      let jsonDictionary = try JSONSerialization.jsonObject(with: self, options: [.mutableContainers]) as? [String: AnyObject]
      return jsonDictionary
    } catch {
      print(error.localizedDescription)
      return nil
    }
  }
  
  func jsonArray() -> [Any]? {
    do {
      let jsonArray = try JSONSerialization.jsonObject(with: self, options: [.mutableContainers]) as? [Any]
      return jsonArray
    } catch {
      print(error.localizedDescription)
      return nil
    }
  }
  
  func jsonString() -> String? {
    do {
      let jsonString = try JSONSerialization.jsonObject(with: self, options: [.mutableContainers]) as? String
      return jsonString
    } catch {
      print(error.localizedDescription)
      return nil
    }
  }
  
  func toString() -> String? {
    if let returnData = String(data: self, encoding: .utf8) {
      return(returnData)
    } else {
      return("")
    }
  }
  
  init<T>(from value: T) {
    self = Swift.withUnsafeBytes(of: value) { Data($0) }
  }
  
  func to<T>(type: T.Type) -> T? where T: ExpressibleByIntegerLiteral {
    var value: T = 0
    guard count >= MemoryLayout.size(ofValue: value) else { return nil }
    _ = Swift.withUnsafeMutableBytes(of: &value, { copyBytes(to: $0)} )
    return value
  }
  
  init<T>(fromArray values: [T]) {
    self = values.withUnsafeBytes { Data($0) }
  }
  
  func toArray<T>(type: T.Type) -> [T] where T: ExpressibleByIntegerLiteral {
    var array = Array<T>(repeating: 0, count: self.count/MemoryLayout<T>.stride)
    _ = array.withUnsafeMutableBytes { copyBytes(to: $0) }
    return array
  }
}
