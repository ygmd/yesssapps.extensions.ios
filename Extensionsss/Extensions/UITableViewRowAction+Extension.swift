//
//  UITableViewRowAction+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UITableViewRowAction {
  
  class func rowAction2(title: String?, titleBorderMargin:Int, font:UIFont, fontColor:UIColor, verticalMargin:CGFloat, image: UIImage, forCellHeight cellHeight: CGFloat,  backgroundColor: UIColor, handler: @escaping (UITableViewRowAction, IndexPath) -> Void) -> UITableViewRowAction {
    
    // clacolo titolo
    var largezzaTesto : Int = 1
    
    if let titleTest = title {
      largezzaTesto = titleTest.count + (titleBorderMargin * 2)
    } else {
      largezzaTesto = titleBorderMargin
    }
    let titleSpaceString = "".padding(toLength: largezzaTesto, withPad: "\u{3000}", startingAt: 0)
    
    let rowAction = UITableViewRowAction(style: .default, title: titleSpaceString, handler: handler)
    
    let larghezzaTestoConSpazio = titleSpaceString.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: cellHeight),
                                                                options: .usesLineFragmentOrigin,
                                                                attributes: [NSAttributedString.Key.font: font],
                                                                context: nil).size.width + 30
    // calcolo grandezza
    let frameGuess: CGSize = CGSize(width: larghezzaTestoConSpazio, height: cellHeight)
    
    let tripleFrame: CGSize = CGSize(width: frameGuess.width * 2.0, height: frameGuess.height * 2.0)
    
    // trucco
    UIGraphicsBeginImageContextWithOptions(tripleFrame, false, UIScreen.main.scale)
    let context: CGContext = UIGraphicsGetCurrentContext()!
    
    backgroundColor.setFill()
    context.fill(CGRect(x: 0, y: 0, width: tripleFrame.width, height: tripleFrame.height))
    
    if let _ = title {
      image.draw(at: CGPoint(x: (frameGuess.width / 2.0) - (image.size.width / 2.0),
                             y: (frameGuess.height / 2.0) - image.size.height - (verticalMargin / 2.0) + 4.0))
    } else {
      image.draw(at: CGPoint( x: (frameGuess.width / 2.0) - (image.size.width / 2.0),
                              y: (frameGuess.height / 2.0) - image.size.height / 2.0) )
    }
    
    if let titleTest = title {
      let drawnTextSize: CGSize = titleTest.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: cellHeight), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil).size
      
      let direction : CGFloat = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? -1 : 1
      
      titleTest.draw(in: CGRect( x: ((frameGuess.width / 2.0) - (drawnTextSize.width / 2.0)) * direction, y: (frameGuess.height / 2.0) + (verticalMargin / 2.0) + 2.0, width: frameGuess.width, height: frameGuess.height), withAttributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: fontColor])
    }
    
    rowAction.backgroundColor = UIColor(patternImage: UIGraphicsGetImageFromCurrentImageContext()!)
    UIGraphicsEndImageContext()
    
    return rowAction
  }
  
}
