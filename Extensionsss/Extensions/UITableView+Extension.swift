//
//  UITableView+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UITableView {
  
  func createBlur(_ effectStyle: UIBlurEffect.Style, withImage image:UIImage?, lineVibrance:Bool) {
    
    if let imageTest = image {
      self.backgroundColor = UIColor(patternImage: imageTest)
    } else {
      self.backgroundColor = UIColor.clear
    }
    
    let blurEffect = UIBlurEffect(style: effectStyle)
    self.backgroundView = UIVisualEffectView(effect: blurEffect)
    if !lineVibrance { return }
    self.separatorEffect = UIVibrancyEffect(blurEffect: blurEffect)
  }
  
  func createNoPaintBlur(_ effectStyle: UIBlurEffect.Style, withImage image:UIImage?, lineVibrance:Bool) {
    
    let blurEffect = UIBlurEffect(style: effectStyle)
    let packView = UIView(frame: self.frame)
    
    if let imageTest = image {
      
      let imVi = UIImageView(frame: packView.frame)
      imVi.contentMode = .scaleToFill
      imVi.image = imageTest
      packView.addSubview(imVi)
      
      let fx = UIVisualEffectView(effect: blurEffect)
      fx.frame = packView.frame
      packView.addSubview(fx)
      
      self.backgroundView = packView
    } else {
      self.backgroundColor = UIColor.clear
      self.backgroundView = UIVisualEffectView(effect: blurEffect)
    }
    
    if !lineVibrance { return }
    self.separatorEffect = UIVibrancyEffect(blurEffect: blurEffect)
  }
}
