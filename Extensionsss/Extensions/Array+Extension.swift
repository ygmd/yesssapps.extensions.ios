//
//  Array+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

public extension Array {
  // Safely lookup an index that might be out of bounds,
  // returning nil if it does not exist
  func get(_ index: Int) -> Element? {
    if 0 <= index && index < count {
      return self[index]
    } else {
      return nil
    }
  }
  
  var only: Element? {
    count == 1 ? first : nil
  }
}

public extension Array where Element: Hashable {
  func removingDuplicates() -> [Element] {
    var addedDict = [Element: Bool]()
    
    return filter {
      addedDict.updateValue(true, forKey: $0) == nil
    }
  }
  
  mutating func removeDuplicates() {
    self = self.removingDuplicates()
  }
  
  func set() -> Set<Array.Element> {
    return Set(self)
  }
  
  func isSubset(of array: Array) -> Bool {
    self.set().isSubset(of: array.set())
  }
  
  func isSuperset(of array: Array) -> Bool {
    self.set().isSuperset(of: array.set())
  }
  
  func hasCommonElements(with array: Array) -> Bool {
    return self.commonElements(between: array).count >= 1 ? true : false
  }
  
  func commonElements(between array: Array) -> Array {
    let intersection = self.set().intersection(array.set())
    return intersection.map({ $0 })
  }
  
  func difference(from other: [Element]) -> [Element] {
    let thisSet = Set(self)
    let otherSet = Set(other)
    return Array(thisSet.symmetricDifference(otherSet))
  }
}

public extension Array where Element: Equatable {
  mutating func removeDuplicates() {
    var result = [Element]()
    for value in self {
      if !result.contains(value) {
        result.append(value)
      }
    }
    self = result
  }
  
  func doesNotContain(_ element: Element) -> Bool {
    return !contains(element)
  }
  
  var unique: [Element] {
    var uniqueValues: [Element] = []
    forEach { item in
      if !uniqueValues.contains(item) {
        uniqueValues += [item]
      }
    }
    return uniqueValues
  }
}

@available(iOS 13, *)
public extension Array where Element: Identifiable {
  func firstIndex(matching: Element) -> Int? {
    for index in 0..<self.count {
      if self[index].id == matching.id {
        return index
      }
    }
    return nil
  }
}

