//
//  NSMutableAttributedString+Extension.swift
//  Extensionsss
//
//  Created by Djamil Secco on 13/12/2019.
//  Copyright © 2019 Yesss Group (W) SA. All rights reserved.
//

import Foundation

// An attributed string extension to achieve colors on text.
public extension NSMutableAttributedString {
  func setColor(color: UIColor, forText stringValue: String) {
    let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
    if (range.location != NSNotFound) {
      self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
  }
  
  func setNormal(forText stringValue: String) {
    let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
    if (range.location != NSNotFound) {
      self.addAttribute(NSAttributedString.Key.font, value: normalFont, range: range)
    }
  }
  
  func setLarge(forText stringValue: String) {
    let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
    if (range.location != NSNotFound) {
      self.addAttribute(NSAttributedString.Key.font, value: largeFont, range: range)
    }
  }
  
  func setLargeBold(forText stringValue: String) {
    let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
    if (range.location != NSNotFound) {
      self.addAttribute(NSAttributedString.Key.font, value: largeBoldFont, range: range)
    }
  }
  
  func setBold(forText stringValue: String) {
    let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
    if (range.location != NSNotFound) {
      self.addAttribute(NSAttributedString.Key.font, value: boldFont, range: range)
    }
  }
  
  func setUnderlined(forText stringValue: String) {
    let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
    if (range.location != NSNotFound) {
      let attributes:[NSAttributedString.Key : Any] = [
          .font :  normalFont,
          .underlineStyle : NSUnderlineStyle.single.rawValue
      ]
      self.addAttributes(attributes, range: range)
    }
  }
  
  func setStrikedThrough(forText stringValue: String) {
    let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
    if (range.location != NSNotFound) {
      let attributes:[NSAttributedString.Key : Any] = [
          .font :  normalFont,
          .strikethroughStyle: NSUnderlineStyle.single.rawValue
      ]
      self.addAttributes(attributes, range: range)
    }
  }
  
  func setOrangeHighlight(forText stringValue: String) {
    let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
    if (range.location != NSNotFound) {
      let attributes:[NSAttributedString.Key : Any] = [
          .font :  normalFont,
          .foregroundColor : UIColor.white,
          .backgroundColor : UIColor.orange
      ]
      self.addAttributes(attributes, range: range)
    }
  }
  
  func setBlackHighlight(forText stringValue: String) {
    let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
    if (range.location != NSNotFound) {
      let attributes:[NSAttributedString.Key : Any] = [
          .font :  normalFont,
          .foregroundColor : UIColor.white,
          .backgroundColor : UIColor.black
      ]
      self.addAttributes(attributes, range: range)
    }
  }
  
  
  func setColor(color: UIColor, forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }
  
  func setNormal(forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        self.addAttribute(NSAttributedString.Key.font, value: normalFont, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }
  
  func setLarge(forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        self.addAttribute(NSAttributedString.Key.font, value: largeFont, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }
  
  func setLargeBold(forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        self.addAttribute(NSAttributedString.Key.font, value: largeBoldFont, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }
  
  func setBold(forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        self.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }
  
  func setUnderlined(forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
        ]
        self.addAttributes(attributes, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }
  
  func setStrikedThrough(forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .strikethroughStyle: NSUnderlineStyle.single.rawValue
        ]
        self.addAttributes(attributes, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }
  
  func setOrangeHighlight(forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        self.addAttributes(attributes, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }
  
  func setBlackHighlight(forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
        ]
        self.addAttributes(attributes, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }
  
  var fontSize:CGFloat { return 14 }
  var largeFontSize:CGFloat { return 20 }
  var boldFont:UIFont { return UIFont(name: "AvenirNext-Bold", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize) }
  var normalFont:UIFont { return UIFont(name: "AvenirNext-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
  var largeFont:UIFont { return UIFont(name: "AvenirNext-Regular", size: largeFontSize) ?? UIFont.systemFont(ofSize: largeFontSize)}
  var largeBoldFont:UIFont { return UIFont(name: "AvenirNext-Bold", size: largeFontSize) ?? UIFont.boldSystemFont(ofSize: largeFontSize)}
  
  func bold(_ value:String) -> NSMutableAttributedString {

      let attributes:[NSAttributedString.Key : Any] = [
          .font : boldFont
      ]

      self.append(NSAttributedString(string: value, attributes:attributes))
      return self
  }

  func normal(_ value:String) -> NSMutableAttributedString {

      let attributes:[NSAttributedString.Key : Any] = [
          .font : normalFont,
      ]

      self.append(NSAttributedString(string: value, attributes:attributes))
      return self
  }
  /* Other styling methods */
  func orangeHighlight(_ value:String) -> NSMutableAttributedString {

      let attributes:[NSAttributedString.Key : Any] = [
          .font :  normalFont,
          .foregroundColor : UIColor.white,
          .backgroundColor : UIColor.orange
      ]

      self.append(NSAttributedString(string: value, attributes:attributes))
      return self
  }

  func blackHighlight(_ value:String) -> NSMutableAttributedString {

      let attributes:[NSAttributedString.Key : Any] = [
          .font :  normalFont,
          .foregroundColor : UIColor.white,
          .backgroundColor : UIColor.black

      ]

      self.append(NSAttributedString(string: value, attributes:attributes))
      return self
  }

  func underlined(_ value:String) -> NSMutableAttributedString {

      let attributes:[NSAttributedString.Key : Any] = [
          .font :  normalFont,
          .underlineStyle : NSUnderlineStyle.single.rawValue

      ]

      self.append(NSAttributedString(string: value, attributes:attributes))
      return self
  }
  
  func strikedThrough(_ value:String) -> NSMutableAttributedString {

      let attributes:[NSAttributedString.Key : Any] = [
          .font :  normalFont,
          .strikethroughStyle: NSUnderlineStyle.single.rawValue
      ]
      self.append(NSAttributedString(string: value, attributes:attributes))
      return self
  }
  
  func setFont(font: UIFont, forAllOccurancesOfText stringValue: String) {
    let inputLength = self.string.count
    let searchLength = stringValue.count
    var range = NSRange(location: 0, length: self.length)
    
    while (range.location != NSNotFound) {
      range = (self.string as NSString).range(of: stringValue, options: [], range: range)
      if (range.location != NSNotFound) {
        self.addAttribute(NSAttributedString.Key.font, value: font, range: NSRange(location: range.location, length: searchLength))
        range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
      }
    }
  }

}
