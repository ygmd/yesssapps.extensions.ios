//
//  Encodable+Extension.swift
//  Extensionsss
//
//  Created by Djamil Secco on 11/05/2022.
//  Copyright © 2022 Yesss Group (W) SA. All rights reserved.
//

import Foundation

struct JSONDATA {
  static var encoder: JSONEncoder {
    get {
      let encoder = JSONEncoder()
      encoder.outputFormatting = .prettyPrinted
      return encoder
    }
  }
}

public extension Encodable {
  subscript(key: String) -> Any? {
    return dictionary[key]
  }
  
  var dictionary: [String: Any] {
    return (try? JSONSerialization.jsonObject(with: JSONDATA.encoder.encode(self))) as? [String: Any] ?? [:]
  }
  
  var dictionaryOfStrings: [String: String] {
    return (try? JSONSerialization.jsonObject(with: JSONDATA.encoder.encode(self))) as? [String: String] ?? [:]
  }
}
