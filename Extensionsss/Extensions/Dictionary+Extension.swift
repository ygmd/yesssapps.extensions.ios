//
//  Dictionary+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

public extension Dictionary {
  func jsonString() -> String? {
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: self, options:[JSONSerialization.WritingOptions.prettyPrinted]) //JSONSerialization.WritingOptions.prettyPrinted
      return String(data: jsonData, encoding: .utf8)
    } catch {
      print(error.localizedDescription)
      return nil
    }
  }
}
