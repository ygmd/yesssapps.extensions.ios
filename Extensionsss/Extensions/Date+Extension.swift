//
//  Date+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

let componentFlags : Set<Calendar.Component> = [Calendar.Component.year, Calendar.Component.month, Calendar.Component.day, Calendar.Component.weekdayOrdinal, Calendar.Component.hour,Calendar.Component.minute, Calendar.Component.second, Calendar.Component.weekday, Calendar.Component.weekdayOrdinal]


public extension Date {
  //Crea una data direttamente dai valori passati
  static func customDate(year ye:Int, month mo:Int, day da:Int, hour ho:Int, minute mi:Int, second se:Int) -> Date {
    var comps = DateComponents()
    comps.year = ye
    comps.month = mo
    comps.day = da
    comps.hour = ho
    comps.minute = mi
    comps.second = se
    let date = Calendar.current.date(from: comps)
    return date!
  }
  
  static func customDateUInt(year ye:UInt, month mo:UInt, day da:UInt, hour ho:UInt, minute mi:UInt, second se:UInt) -> Date {
    var comps = DateComponents()
    comps.year = Int(ye)
    comps.month = Int(mo)
    comps.day = Int(da)
    comps.hour = Int(ho)
    comps.minute = Int(mi)
    comps.second = Int(se)
    let date = Calendar.current.date(from: comps)
    return date!
  }
  
  static func dateOfMonthAgo() -> Date {
    return Date().addingTimeInterval(-24 * 30 * 60 * 60)
  }
  
  static func dateOfWeekAgo() -> Date {
    return Date().addingTimeInterval(-24 * 7 * 60 * 60)
  }
  
  func sameDate(ofDate:Date) -> Bool {
    let cal = Calendar.current
    let dif = cal.compare(self, to: ofDate, toGranularity: Calendar.Component.day)
    if dif == .orderedSame {
      return true
    } else {
      return false
    }
  }
  
  static func currentCalendar() -> Calendar {
    return Calendar.autoupdatingCurrent
  }
  
  func isEqualToDateIgnoringTime(_ aDate:Date) -> Bool {
    let components1 = Date.currentCalendar().dateComponents(componentFlags, from: self)
    let components2 = Date.currentCalendar().dateComponents(componentFlags, from: aDate)
    
    return ((components1.year == components2.year) &&
      (components1.month == components2.month) &&
      (components1.day == components2.day))
  }
  
  func plusSeconds(_ s: UInt) -> Date {
    return self.addComponentsToDate(seconds: Int(s), minutes: 0, hours: 0, days: 0, weeks: 0, months: 0, years: 0)
  }
  
  func minusSeconds(_ s: UInt) -> Date {
    return self.addComponentsToDate(seconds: -Int(s), minutes: 0, hours: 0, days: 0, weeks: 0, months: 0, years: 0)
  }
  
  func plusMinutes(_ m: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: Int(m), hours: 0, days: 0, weeks: 0, months: 0, years: 0)
  }
  
  func minusMinutes(_ m: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: -Int(m), hours: 0, days: 0, weeks: 0, months: 0, years: 0)
  }
  
  func plusHours(_ h: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: Int(h), days: 0, weeks: 0, months: 0, years: 0)
  }
  
  func minusHours(_ h: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: -Int(h), days: 0, weeks: 0, months: 0, years: 0)
  }
  
  func plusDays(_ d: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: Int(d), weeks: 0, months: 0, years: 0)
  }
  
  func minusDays(_ d: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: -Int(d), weeks: 0, months: 0, years: 0)
  }
  
  func plusWeeks(_ w: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: Int(w), months: 0, years: 0)
  }
  
  func minusWeeks(_ w: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: -Int(w), months: 0, years: 0)
  }
  
  func plusMonths(_ m: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: 0, months: Int(m), years: 0)
  }
  
  func minusMonths(_ m: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: 0, months: -Int(m), years: 0)
  }
  
  func plusYears(_ y: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: 0, months: 0, years: Int(y))
  }
  
  func minusYears(_ y: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: 0, months: 0, years: -Int(y))
  }
  
  private func addComponentsToDate(seconds sec: Int, minutes min: Int, hours hrs: Int, days d: Int, weeks wks: Int, months mts: Int, years yrs: Int) -> Date {
    var dc:DateComponents = DateComponents()
    dc.second = sec
    dc.minute = min
    dc.hour = hrs
    dc.day = d
    dc.weekOfYear = wks
    dc.month = mts
    dc.year = yrs
    return Calendar.current.date(byAdding: dc, to: self, wrappingComponents: false)!
  }
  
  func midnightUTCDate() -> Date {
    var dc:DateComponents = Calendar.current.dateComponents([Calendar.Component.year, Calendar.Component.month, Calendar.Component.day], from: self)
    dc.hour = 0
    dc.minute = 0
    dc.second = 0
    dc.nanosecond = 0
    (dc as NSDateComponents).timeZone = TimeZone(secondsFromGMT: 0)
    
    return Calendar.current.date(from: dc)!
  }
  
  static func secondsBetween(date1 d1:Date, date2 d2:Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.second!
  }
  
  static func minutesBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.minute!
  }
  
  static func hoursBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.hour!
  }
  
  static func daysBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.day!
  }
  
  static func weeksBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.weekOfYear!
  }
  
  static func monthsBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.month!
  }
  
  static func yearsBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.year!
  }
  
  //MARK- Comparison Methods
  func isGreaterThan(_ date: Date) -> Bool {
    return (self.compare(date) == .orderedDescending)
  }
  
  func isLessThan(_ date: Date) -> Bool {
    return (self.compare(date) == .orderedAscending)
  }
  
  //MARK- Computed Properties
  var day: UInt {
    return UInt(Calendar.current.component(.day, from: self))
  }
  
  var weekday: UInt {
    return UInt(Calendar.current.component(.weekday, from: self))
  }
  
  var month: UInt {
    return UInt(Calendar.current.component(.month, from: self))
  }
  
  var year: UInt {
    return UInt(Calendar.current.component(.year, from: self))
  }
  
  var hour: UInt {
    return UInt(Calendar.current.component(.hour, from: self))
  }
  
  var minute: UInt {
    return UInt(Calendar.current.component(.minute, from: self))
  }
  
  var second: UInt {
    return UInt(Calendar.current.component(.second, from: self))
  }
  
  func iso8601() -> String {
    let currentCountryCode = Locale.current.regionCode ?? "GB"
    let currentLanguageCode = Locale.current.languageCode ?? "en"
    let localeIdentifier = "\(currentLanguageCode)_\(currentCountryCode)_POSIX"
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.init(identifier: localeIdentifier)
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
    return dateFormatter.string(from: self)
  }
  
  //MARK - Date -> String
  func stringWithFormat(_ format: String) -> String {
    let currentCountryCode = Locale.current.regionCode ?? "GB"
    let currentLanguageCode = Locale.current.languageCode ?? "en"
    let localeIdentifier = "\(currentLanguageCode)_\(currentCountryCode)_POSIX"
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.init(identifier: localeIdentifier)
    dateFormatter.dateFormat = format
    return dateFormatter.string(from: self)
  }
  
  var dayName: String {
    let currentCountryCode = Locale.current.regionCode ?? "GB"
    let currentLanguageCode = Locale.current.languageCode ?? "en"
    let localeIdentifier = "\(currentLanguageCode)_\(currentCountryCode)_POSIX"
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.init(identifier: localeIdentifier)
    dateFormatter.dateFormat = "EEEE"
    return dateFormatter.string(from: self)
  }
  
  var dayNameShort: String {
    let currentCountryCode = Locale.current.regionCode ?? "GB"
    let currentLanguageCode = Locale.current.languageCode ?? "en"
    let localeIdentifier = "\(currentLanguageCode)_\(currentCountryCode)_POSIX"
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.init(identifier: localeIdentifier)
    dateFormatter.dateFormat = "EE"
    return dateFormatter.string(from: self)
  }
  
  var monthName: String {
    let currentCountryCode = Locale.current.regionCode ?? "GB"
    let currentLanguageCode = Locale.current.languageCode ?? "en"
    let localeIdentifier = "\(currentLanguageCode)_\(currentCountryCode)_POSIX"
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.init(identifier: localeIdentifier)
    dateFormatter.dateFormat = "LLLL"
    return dateFormatter.string(from: self)
  }
  
  var monthNameShort: String {
    let currentCountryCode = Locale.current.regionCode ?? "GB"
    let currentLanguageCode = Locale.current.languageCode ?? "en"
    let localeIdentifier = "\(currentLanguageCode)_\(currentCountryCode)_POSIX"
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.init(identifier: localeIdentifier)
    dateFormatter.dateFormat = "LLL"
    return dateFormatter.string(from: self)
  }
  
  // Convert local time to UTC (or GMT)
  func toGlobalTime() -> Date {
    let timezone = TimeZone.current
    let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
    return Date(timeInterval: seconds, since: self)
  }
  
  // Convert UTC (or GMT) to local time
  func toLocalTime() -> Date {
    
    // 1) Get the current TimeZone's seconds from GMT. Since I am in Chicago this will be: 60*60*5 (18000)
    let timezoneOffset = TimeZone.current.secondsFromGMT()
    
    // 2) Get the current date (GMT) in seconds since 1970. Epoch datetime.
    let epochDate = self.timeIntervalSince1970
    
    // 3) Perform a calculation with timezoneOffset + epochDate to get the total seconds for the
    //    local date since 1970.
    //    This may look a bit strange, but since timezoneOffset is given as -18000.0, adding epochDate and timezoneOffset
    //    calculates correctly.
    let timezoneEpochOffset = (epochDate + Double(timezoneOffset))
    
    
    // 4) Finally, create a date using the seconds offset since 1970 for the local date.
    return Date(timeIntervalSince1970: timezoneEpochOffset)
  }
  
}

func ==(lhs: NSDate, rhs: NSDate) -> Bool {
  return lhs === rhs || lhs.compare(rhs as Date) == .orderedSame
}

func <(lhs: NSDate, rhs: NSDate) -> Bool {
  return lhs.compare(rhs as Date) == .orderedAscending
}

func >(lhs: NSDate, rhs: NSDate) -> Bool {
  return lhs.compare(rhs as Date) == .orderedDescending
}

func delay(_ delay:Double, closure: @escaping ()->()) {
  
  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
    closure()
  }
}
