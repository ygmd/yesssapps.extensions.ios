//
//  UITableViewController+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UITableViewController {
  
  func createNoPaintBlur(_ effectStyle: UIBlurEffect.Style, withImage image:UIImage?, lineVibrance:Bool) {
    
    let blurEffect = UIBlurEffect(style: effectStyle)
    let packView = UIView(frame: tableView.frame)
    
    if let imageTest = image {
      
      let imVi = UIImageView(frame: packView.frame)
      imVi.contentMode = .scaleToFill
      imVi.image = imageTest
      packView.addSubview(imVi)
      
      let fx = UIVisualEffectView(effect: blurEffect)
      fx.frame = packView.frame
      packView.addSubview(fx)
      
      tableView.backgroundView = packView
    } else {
      tableView.backgroundColor = UIColor.clear
      tableView.backgroundView = UIVisualEffectView(effect: blurEffect)
    }
    
    if let popover = navigationController?.popoverPresentationController {
      popover.backgroundColor = UIColor.clear
    }
    
    if !lineVibrance { return }
    tableView.separatorEffect = UIVibrancyEffect(blurEffect: blurEffect)
  }
  
  func createBlur(_ effectStyle:UIBlurEffect.Style, withImage image:UIImage?, lineVibrance:Bool) {
    
    if let imageTest = image {
      tableView.backgroundColor = UIColor(patternImage: imageTest)
    } else {
      tableView.backgroundColor = UIColor.clear
    }
    
    if let popover = navigationController?.popoverPresentationController {
      popover.backgroundColor = UIColor.clear
    }
    
    let blurEffect = UIBlurEffect(style: effectStyle)
    tableView.backgroundView = UIVisualEffectView(effect: blurEffect)
    if !lineVibrance { return }
    tableView.separatorEffect = UIVibrancyEffect(blurEffect: blurEffect)
  }
}
