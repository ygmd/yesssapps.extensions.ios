//
//  View+Extension.swift
//  Extensionsss
//
//  Created by Djamil Secco on 11/05/2022.
//  Copyright © 2022 Yesss Group (W) SA. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, *)
public extension View {
  
  func navigationBarColor(_ backgroundColor: UIColor?) -> some View {
    modifier(NavigationBarModifier(backgroundColor: backgroundColor))
  }
  
  func inExpandingRectangle(alignment: Alignment) -> some View {
    ZStack(alignment: alignment) {
      Rectangle()
        .fill(Color.clear)
      self
    }
  }
  
  /// Fix the SwiftUI bug for onAppear twice in subviews
  /// - Parameters:
  ///   - perform: perform the action when appear
  func onFirstAppear(perform action: @escaping () -> Void) -> some View {
    let kAppearAction = "appear_action"
    let queue = OperationQueue.main
    let delayOperation = BlockOperation {
      Thread.sleep(forTimeInterval: 0.001)
    }
    let appearOperation = BlockOperation {
      action()
    }
    appearOperation.name = kAppearAction
    appearOperation.addDependency(delayOperation)
    return onAppear {
      if !delayOperation.isFinished, !delayOperation.isExecuting {
        queue.addOperation(delayOperation)
      }
      if !appearOperation.isFinished, !appearOperation.isExecuting {
        queue.addOperation(appearOperation)
      }
    }
    .onDisappear {
      queue.operations
        .first { $0.name == kAppearAction }?
        .cancel()
    }
  }
  
  func onLoad(perform action: (() -> Void)? = nil) -> some View {
    modifier(ViewDidLoadModifier(perform: action))
  }
  
  func previewWithName(_ name: String) -> some View {
    self
      .padding()
      .previewLayout(.sizeThatFits)
      .previewDisplayName(name)
  }
  
  func namedPreview() -> some View {
    let name = String.name(for: type(of: self))
    return previewWithName(name)
  }
  
//  func fullScreenPreviews(showAll: Bool = false) -> some View {
//    Group {
//      if !showAll {
//        self
//      } else {
//        ForEach(Theme.allThemes) { theme in
//          ForEach(ColorScheme.allCases) { colorScheme in
//            self
//              .previewTheme(theme, colorScheme: colorScheme)
//          }
//        }
//        self
//          .xxxlPreview()
//        self
//          .iPhoneSEPreview()
//      }
//    }
//  }
  
//  func previewTheme(_ theme: Theme, colorScheme: ColorScheme) -> some View {
//    self
//      .background(Color(.systemBackground))
//      .environment(\.theme, theme)
//      .accentColor(theme.accentColor)
//      .previewDisplayName(theme.name + ", " + colorScheme.name)
//      .environment(\.colorScheme, colorScheme)
//  }
  
  func xxxlPreview() -> some View {
    self
      .environment(\.sizeCategory, .accessibilityExtraExtraExtraLarge)
      .previewDisplayName("Accessibility XXXL")
  }
  
  func iPhoneSEPreview() -> some View {
    self
      .previewDevice("iPhone SE (2nd generation)")
      .previewDisplayName("iPhone SE (2nd generation)")
  }
  
  func `if`<Content: View>(_ conditional: Bool, content: (Self) -> Content) -> some View {
    if conditional {
      return AnyView(content(self))
    } else {
      return AnyView(self)
    }
  }
  
  // This function changes our View to UIView, then calls another function
  // to convert the newly-made UIView to a UIImage.
  func asUIImage() -> UIImage {
    let controller = UIHostingController(rootView: self)
    
    controller.view.frame = CGRect(x: 0, y: CGFloat(Int.max), width: 1, height: 1)
    UIApplication.shared.keyWindowPresentedController!.view.addSubview(controller.view)
    
    let size = controller.sizeThatFits(in: UIScreen.main.bounds.size)
    controller.view.bounds = CGRect(origin: .zero, size: size)
    controller.view.sizeToFit()
    
    // here is the call to the function that converts UIView to UIImage: `.asUIImage()`
    let image = controller.view.asUIImage()
    controller.view.removeFromSuperview()
    return image
  }
  
  func interactiveDismiss(canDismissSheet: Bool, onDismissalAttempt: (() -> ())? = nil) -> some View {
    ModalView(
      view: self,
      canDismissSheet: canDismissSheet,
      onDismissalAttempt: onDismissalAttempt
    ).edgesIgnoringSafeArea(.all)
  }
  
}

@available(iOS 13.0.0, *)
struct ViewDidLoadModifier: ViewModifier {
  
  @State private var didLoad = false
  private let action: (() -> Void)?
  
  init(perform action: (() -> Void)? = nil) {
    self.action = action
  }
  
  func body(content: Content) -> some View {
    content.onAppear {
      if didLoad == false {
        didLoad = true
        action?()
      }
    }
  }
  
}

@available(iOS 13.0.0, *)
class ModalHostingController<Content: View>: UIHostingController<Content>, UIAdaptivePresentationControllerDelegate {
  var canDismissSheet = true
  var onDismissalAttempt: (() -> ())?

  override func willMove(toParent parent: UIViewController?) {
    super.willMove(toParent: parent)

    parent?.presentationController?.delegate = self
  }

  func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
    canDismissSheet
  }

  func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
    onDismissalAttempt?()
  }
}

@available(iOS 13.0.0, *)
struct ModalView<T: View>: UIViewControllerRepresentable {
  let view: T
  let canDismissSheet: Bool
  let onDismissalAttempt: (() -> ())?

  func makeUIViewController(context: Context) -> ModalHostingController<T> {
    let controller = ModalHostingController(rootView: view)

    controller.canDismissSheet = canDismissSheet
    controller.onDismissalAttempt = onDismissalAttempt

    return controller
  }

  func updateUIViewController(_ uiViewController: ModalHostingController<T>, context: Context) {
    uiViewController.rootView = view

    uiViewController.canDismissSheet = canDismissSheet
    uiViewController.onDismissalAttempt = onDismissalAttempt
  }
}

@available(iOS 13.0.0, *)
struct NavigationBarModifier: ViewModifier {
  
  var backgroundColor: UIColor?
  
  init( backgroundColor: UIColor?) {
    self.backgroundColor = backgroundColor
    let coloredAppearance = UINavigationBarAppearance()
    coloredAppearance.configureWithTransparentBackground()
    coloredAppearance.backgroundColor = .clear
    //coloredAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
    //coloredAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
    coloredAppearance.titleTextAttributes = [
      .foregroundColor: UIColor.white,
      .font: UIFont(name: "Ford Antenna Cond Bold", size: 20) ?? UIFont.monospacedSystemFont(ofSize: 20, weight: .black)
    ]
    coloredAppearance.largeTitleTextAttributes = [
      .foregroundColor: UIColor.white,
      .font: UIFont(name: "Ford Antenna Cond Bold", size: 36) ?? UIFont.monospacedSystemFont(ofSize: 36, weight: .black)
    ]
    
    UINavigationBar.appearance().standardAppearance = coloredAppearance
    UINavigationBar.appearance().compactAppearance = coloredAppearance
    UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
    //UINavigationBar.appearance().tintColor = .black
  }
  
  func body(content: Content) -> some View {
    ZStack{
      content
      VStack {
        GeometryReader { geometry in
          Color(backgroundColor ?? .clear)
            .frame(height: geometry.safeAreaInsets.top)
            .edgesIgnoringSafeArea(.top)
          Spacer()
        }
      }
    }
  }
}
