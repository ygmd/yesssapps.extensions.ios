//
//  Collection+Dictionary.swift
//  Extensionsss
//
//  Created by Djamil Secco on 11/05/2022.
//  Copyright © 2022 Yesss Group (W) SA. All rights reserved.
//

import SwiftUI

extension Collection {
  func dictionary<K, V>(transform:(_ element: Iterator.Element) -> [K : V]) -> [K : V] {
    var dictionary = [K : V]()
    forEach { element in
      for (key, value) in transform(element) {
        dictionary[key] = value
      }
    }
    return dictionary
  }
}
