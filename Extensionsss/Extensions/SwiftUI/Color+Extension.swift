//
//  Color+Extension.swift
//  Extensionsss
//
//  Created by Djamil Secco on 11/05/2022.
//  Copyright © 2022 Yesss Group (W) SA. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, *)
public extension Color {
  
  init(_ hex: UInt64, opacity: Double = 1.0) {
    let red = Double((hex & 0xff0000) >> 16) / 255.0
    let green = Double((hex & 0xff00) >> 8) / 255.0
    let blue = Double((hex & 0xff) >> 0) / 255.0
    self.init(.sRGB, red: red, green: green, blue: blue, opacity: opacity)
  }
  
  init(hex: String, opacity: Double = 1.0) {
    // convert the String into an UInt64 and then convert into Color
    var hexFormatted: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
    if hexFormatted.hasPrefix("#") {
      hexFormatted = String(hexFormatted.dropFirst())
    }
    assert(hexFormatted.count == 6, "Invalid hex code used.")
    var rgbValue: UInt64 = 0
    Scanner(string: hexFormatted).scanHexInt64(&rgbValue)
    self.init(rgbValue, opacity: opacity)
  }
  
  var hexValue: String? {
    get {
      var drawHexNumber: String = "#FF0000"
      
      let colorString = self.description
      let colorArray: [String] = colorString.components(separatedBy: " ")
      if colorArray.count > 1 {
        var r: CGFloat = CGFloat((Float(colorArray[1]) ?? 1))
        var g: CGFloat = CGFloat((Float(colorArray[2]) ?? 1))
        var b: CGFloat = CGFloat((Float(colorArray[3]) ?? 1))
        
        if (r < 0.0) {r = 0.0}
        if (g < 0.0) {g = 0.0}
        if (b < 0.0) {b = 0.0}
        
        if (r > 1.0) {r = 1.0}
        if (g > 1.0) {g = 1.0}
        if (b > 1.0) {b = 1.0}
        
        // Update hex
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        drawHexNumber = String(format: "#%06X", rgb)
        return drawHexNumber
      } else {
        return nil
      }
    }
  }
  
  static var themeTextField: Color {
    return Color(red: 220.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, opacity: 1.0)
  }
  
  static var companyBlue: Color {
    Color("companyBlue")
  }
  
  static var companyBlueDark: Color {
    Color("companyBlueDark")
  }
  
  static var companyGreen: Color {
    Color("companyGreen")
  }
  
  static var companyYellow: Color {
    Color("companyYellow")
  }
  
  static var companyBlack: Color {
    Color("companyBlack")
  }
  
  static var companyWhite: Color {
    Color("companyWhite")
  }
  
  static var companyGray: Color {
    Color("companyGray")
  }
  
  static var companyViolet: Color {
    Color("companyViolet")
  }
  
  static var companyVioletDark: Color {
    Color("companyVioletDark")
  }
  
  static var random: Color {
    let colors: [Color] = [Color("blue"), Color("brown"), Color("cyan"), Color("gray"), Color("green"), Color("indigo"), Color("mint"), Color("orange"), Color("pink"), Color("purple"), Color("red"), Color("teal"), Color("yellow")]
    return colors.randomElement()!
  }
  
  static var randomName: String {
    let colorNames: [String] = ["blue", "brown", "cyan", "gray", "green", "indigo", "mint", "orange", "pink", "purple", "red", "teal", "yellow"]
    return colorNames.randomElement()!
  }
  
  // YESSS
  static let yesssBlue = Color(red: 0.0 / 255.0, green: 156.0 / 255.0, blue: 255.0 / 255.0, opacity: 1)
  static let yesssYellow = Color(red: 255.0 / 255.0, green: 228.0 / 255.0, blue: 0.0 / 255.0, opacity: 1)
  static let yesssGreen = Color(red: 138.0 / 255.0, green: 204.0 / 255.0, blue: 17.0 / 255.0, opacity: 1)
  
  // Blue
  static let riderDarkBlueColor = Color(red: 50.0 / 255.0, green: 49.0 / 255.0, blue: 94.0 / 255.0, opacity: 1.0)         // #32315E
  static let riderBlueColor = Color(red: 0.0 / 255.0, green: 144.0 / 255.0, blue: 250.0 / 255.0, opacity: 1.0)            // #0090FA
  
  // Gray
  static let riderDarkGrayColor = Color(red: 135.0 / 255.0, green: 152.0 / 255.0, blue: 171.0 / 255.0, opacity: 1.0)      // #8798AB
  static let riderGrayColor = Color(red: 170.0 / 255.0, green: 183.0 / 255.0, blue: 197.0 / 255.0, opacity: 1.0)          // #AAB7C5
  static let riderLightGrayColor = Color(red: 233.0 / 255.0, green: 238.0 / 255.0, blue: 245.0 / 255.0, opacity: 1.0)     // #E9EEF5
  static let riderVeryLightGrayColor = Color(red: 246.0 / 255.0, green: 249.0 / 255.0, blue: 252.0 / 255.0, opacity: 1.0) // #F6F9FC
  
  // Green
  static let riderGreenColor = Color(red: 19.0 / 255.0, green: 181.0 / 255.0, blue: 125.0 / 255.0, opacity: 1.0)          // #13B57D
  static let riderLightGreenColor = Color(red: 173.0 / 255.0, green: 242.0 / 255.0, blue: 180.0 / 255.0, opacity: 1.0)    // #ADF2B4
  
  // Red
  static let darkRed = Color(red:0.25, green:0.25, blue:0.29, opacity:1.00)
  
  // Alert
  static let cdalertGreen = Color(red: 65/255, green: 158/255, blue: 57/255, opacity: 1)
  static let cdalertRed = Color(red: 235/255, green: 61/255, blue: 65/255, opacity: 1)
  static let cdalertYellow = Color(red: 255/255, green: 149/255, blue: 0/255, opacity: 1)
  static let cdalertBlue = Color(red: 27/255, green: 169/255, blue: 225/255, opacity: 1)
  static let cdalertAlarm = Color(red: 196/255, green: 52/255, blue: 46/255, opacity: 1)
  
  // Metallic
  static let gold1 = Color(red: 218.0 / 255.0, green: 165.0 / 255.0, blue: 32.0 / 255.0, opacity: 1)
  static let gold2 = Color(red: 212.0 / 255.0, green: 175.0 / 255.0, blue: 55.0 / 255.0, opacity: 1)
  static let gold3 = Color(red: 212.0 / 255.0, green: 175.0 / 255.0, blue: 17.0 / 55.0, opacity: 1)
  static let silver = Color(red: 192.0 / 255.0, green: 192.0 / 255.0, blue: 192.0 / 255.0, opacity: 1)
  static let bronze = Color(red: 80.0 / 255.0, green: 50.0 / 255.0, blue: 20.0 / 255.0, opacity: 1)
  
  // Themes
  static let halloweenOrange = Color(red: 1.0 / 255.0, green: 165.0 / 255.0, blue: 26.0 / 255.0, opacity: 1)
  static let saintPatrickGreen = Color(red: 1.0 / 255.0, green: 165.0 / 255.0, blue: 26.0 / 255.0, opacity: 1)
}
