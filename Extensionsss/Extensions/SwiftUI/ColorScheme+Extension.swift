//
//  ColorScheme+Extension.swift
//  Extensionsss
//
//  Created by Djamil Secco on 11/05/2022.
//  Copyright © 2022 Yesss Group (W) SA. All rights reserved.
//

import SwiftUI

// MARK: - ColorScheme
@available(iOS 13.0, *)
extension ColorScheme: Identifiable {
  public var id: String { name }
  
  var name: String {
    switch self {
    case .light: return "Light"
    case .dark: return "Dark"
    @unknown default:
      fatalError()
    }
  }
}
