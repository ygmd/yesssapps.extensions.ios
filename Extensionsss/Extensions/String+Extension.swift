//
//  String+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension String {
  
  //  var capitalizedFirstLetter:String {
  //    let string = self
  //    return string.replacingCharacters(in: startIndex...startIndex, with: String(self[startIndex]).capitalized)
  //  }
  
  func contains(find: String) -> Bool{
    return self.range(of: find) != nil
  }
  
  func containsIgnoringCase(find: String) -> Bool{
    return self.range(of: find, options: .caseInsensitive) != nil
  }
  
  func toBool() -> Bool {
    return ["1", "y", "true"].contains(self.lowercased())
  }
  
  func captalizeFirstCharacter() -> String {
    var result = self
    let substr1 = String(self[startIndex]).uppercased()
    result.replaceSubrange(...startIndex, with: substr1)
    return result
  }
  
  var length : Int {
    return self.count
  }
  
  func words(with charset: CharacterSet = .alphanumerics) -> [String] {
    return self.unicodeScalars.split {
      !charset.contains($0)
      }.map(String.init)
  }
  
  func wrapped(after: Int = 70) -> String {
    var i = 0
    let lines = self.split(omittingEmptySubsequences: false) { character in
      switch character {
      case "\n",
           " " where i >= after:
        i = 0
        return true
      default:
        i += 1
        return false
      }
      }.map(String.init)
    return lines.joined(separator: "\n")
  }
  
  subscript (i: Int) -> Character {
    return self[self.index(self.startIndex, offsetBy: i)]
  }
  
  subscript (i: Int) -> String {
    return String(self[i] as Character)
  }
  
  subscript (r: Range<Int>) -> String {
    let start = self.index(self.startIndex, offsetBy: r.lowerBound)
    let end = self.index(self.startIndex, offsetBy: r.upperBound)
    
    return String(self[start...end])
  }
  
  var localized: String {
    return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
  }
  
  func loc(_ localizedKey:String) -> String {
    return NSLocalizedString(localizedKey, comment: "")
  }
  
  var urlEscapedHost: String {
    return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
  }
  
  var urlEscapedPath: String {
    return addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
  }
  
  var urlEscapedQuery: String {
    return addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
  }
  
  var utf8Encoded: Data {
    return data(using: .utf8)!
  }
  
  var urlEscaped : String {
    return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
  }
  
  func urlEncode() ->  String {
    let unreserved = "*-._~" // "!*'\"();:@&=+$,/?%#[]%- "
    var allowed = CharacterSet.alphanumerics
    allowed.insert(charactersIn: unreserved) // as per RFC 3986
    return self.addingPercentEncoding(withAllowedCharacters: allowed)!
  }
  
  
  var URLEncodedString: String {
    var allowedCharacterSet = NSMutableCharacterSet.urlQueryAllowed
    allowedCharacterSet.remove(charactersIn: "\n:#/?@!$&'()*+,;=") // these characters need to be left alone :p
    return self.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)!
  }
  
  func left(numberOfCharacters: Int) -> String {
    guard numberOfCharacters <= self.length else { return self }
    let index = self.index(self.startIndex, offsetBy: numberOfCharacters)
    return String(self[..<index])
  }
  
  func right(numberOfCharacters: Int) -> String {
    guard numberOfCharacters <= self.length else { return self }
    let index = self.index(self.endIndex, offsetBy: -numberOfCharacters)
    return String(self[index...])
  }
  
  func trimmed() -> String {
    return self.trimmingCharacters(in: .whitespacesAndNewlines)
  }
  
  func removeString(_ string: String) -> String {
    return self.replacingOccurrences(of: string, with: "")
  }
  
  func removeWhitespaces() -> String {
    return self.replacingOccurrences(of: " ", with: "")
  }
  
  func replaceCommaWithDot() -> String {
    return self.replacingOccurrences(of: ",", with: ".")
  }
  
  //: ### Base64 --> UIImage
  func base64ToImage() -> UIImage {
    var img: UIImage = #imageLiteral(resourceName: "placeholder") //UIImage()
    if (!self.isEmpty) {
      let decodedData:NSData = NSData(base64Encoded: self, options:.ignoreUnknownCharacters)!
      let decodedimage:UIImage = UIImage(data: decodedData as Data)!
      img = decodedimage
    }
    return img
  }
  
  //: ### Base64 decoding a string
  func fromBase64() -> String? {
    guard let data = Data(base64Encoded: self) else {
      return nil
    }
    return String(data: data, encoding: .utf8)
  }
  
  //: ### Base64 encoding a string
  func toBase64() -> String {
    return Data(self.utf8).base64EncodedString()
  }
  
  func base64Encoded() -> String? {
    if let data = self.data(using: .utf8) {
      return data.base64EncodedString()
    }
    return nil
  }
  
  func base64Decoded() -> String? {
    if let data = Data(base64Encoded: self) {
      return String(data: data, encoding: .utf8)
    }
    return nil
  }
  
  func padding(length: Int) -> String {
    return self.padding(toLength: length, withPad: " ", startingAt: 0)
  }
  
  func padding(length: Int, paddingString: String) -> String {
    return self.padding(toLength: length, withPad: paddingString, startingAt: 0)
  }
  
  func leftPadding(toLength: Int, withPad character: Character) -> String {
    let newLength = self.count
    if newLength < toLength {
      return String(repeatElement(character, count: toLength - newLength)) + self
    } else {
      let index = self.index(self.startIndex, offsetBy: newLength - toLength)
      return String(self[index...])
    }
  }
  
  func isEmail() -> Bool {
    do {
      let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
      return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
    } catch {
      return false
    }
  }
  
  // String -> Date
  func toDate(withFormat format: String) -> Date {
    let currentCountryCode = Locale.current.regionCode ?? "GB"
    let currentLanguageCode = Locale.current.languageCode ?? "en"
    let localeIdentifier = "\(currentLanguageCode)_\(currentCountryCode)_POSIX"
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.init(identifier: localeIdentifier)
    dateFormatter.dateFormat = format
    return dateFormatter.date(from: self)!
  }
  
  func iso8601ToDate() -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    return dateFormatter.date(from: self)!
  }
  
  // String -> Double
  func toDouble(locale: Locale?=nil, fractionDigits: Int?=nil) -> Double {
    let numberformatter = NumberFormatter()
    numberformatter.locale = locale ?? Locale.current
    numberformatter.numberStyle = .decimal
    numberformatter.minimumFractionDigits = fractionDigits ?? 2
    numberformatter.maximumFractionDigits = fractionDigits ?? 2
    guard let numberFromNumberFormatter = numberformatter.number(from: self) else {
      return 0.00
    }
    return numberFromNumberFormatter.doubleValue
  }
  
  // String -> Int
  func toInt(locale: Locale?=nil) -> Int {
    let numberformatter = NumberFormatter()
    numberformatter.locale = locale ?? Locale.current
    guard let numberFromNumberFormatter = numberformatter.number(from: self) else {
      return 0
    }
    return numberFromNumberFormatter.intValue
  }
  
  // String -> Float
  func toFloat(locale: Locale?=nil, fractionDigits: Int?=nil) -> Float {
    let numberformatter = NumberFormatter()
    numberformatter.locale = locale ?? Locale.current
    numberformatter.numberStyle = .decimal
    numberformatter.minimumFractionDigits = fractionDigits ?? 2
    numberformatter.maximumFractionDigits = fractionDigits ?? 2
    guard let numberFromNumberFormatter = numberformatter.number(from: self) else {
      return 0
    }
    return numberFromNumberFormatter.floatValue
  }
  
  // String -> UInt
  func toUInt(locale: Locale?=nil) -> UInt {
    let numberformatter = NumberFormatter()
    numberformatter.locale = locale ?? Locale.current
    guard let numberFromNumberFormatter = numberformatter.number(from: self) else {
      return 0
    }
    return numberFromNumberFormatter.uintValue
  }
  
  // String -> JSON String
  func toJSonString(data : Any) -> String {
    var jsonString = "";
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
      jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
    } catch {
      print(error.localizedDescription)
    }
    return jsonString;
  }
  
  var isValidURL: Bool {
    let regEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
    let predicate = NSPredicate(format: "SELF MATCHES %@", argumentArray: [regEx])
    return predicate.evaluate(with: self)
  }
  
  var isValidYoutubeLink: Bool {
    //    let regEx = "(https://(www|m){0,1}.youtube.com/(watch|playlist)[?]v=[a-zA-Z0-9_-]+)"
    //    let predicate = NSPredicate(format: "SELF MATCHES %@", argumentArray: [regEx])
    //    return predicate.evaluate(with: self)
    
    let youtubeRegex = "(http(s)?:\\/\\/)?(www\\.|m\\.)?youtu(be\\.com|\\.be)(\\/watch\\?([&=a-z]{0,})(v=[\\d\\w]{1,}).+|\\/[\\d\\w]{1,})"
    let youtubeCheckResult = NSPredicate(format: "SELF MATCHES %@", youtubeRegex)
    return youtubeCheckResult.evaluate(with: self)
  }
  
  var removeOddQuotes: String {
    var stringTmp = self
    stringTmp = stringTmp.replacingOccurrences(of: "`", with: "'")
    stringTmp = stringTmp.replacingOccurrences(of: "‘", with: "'")
    stringTmp = stringTmp.replacingOccurrences(of: "’", with: "'")
    stringTmp = stringTmp.replacingOccurrences(of: "'", with: "'")
    
    stringTmp = stringTmp.replacingOccurrences(of: "«", with: "\"")
    stringTmp = stringTmp.replacingOccurrences(of: "»", with: "\"")
    stringTmp = stringTmp.replacingOccurrences(of: "„", with: "\"")
    stringTmp = stringTmp.replacingOccurrences(of: "“", with: "\"")
    stringTmp = stringTmp.replacingOccurrences(of: "”", with: "\"")
    
    stringTmp = stringTmp.replacingOccurrences(of: "…", with: "...")
    
    stringTmp = stringTmp.replacingOccurrences(of: "œ", with: "oe")
    stringTmp = stringTmp.replacingOccurrences(of: "Œ", with: "OE")
    
    return stringTmp
  }
  
  func checkLinks() -> String {
    var text = self
    let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
    let matches = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count))
    var originals = [String]()
    var replacements = [String]()
    for match in matches {
      guard let range = Range(match.range, in: text) else { continue }
      let prefix = String(text[range]).left(numberOfCharacters: 4) == "http" ? "" : "https://"
      let formattedLink = "<a href=\"\(prefix)\(text[range])\">\(text[range])</a>"
      originals.append(String(text[range]))
      replacements.append(formattedLink)
    }
    for (idx, unformattedLink) in originals.enumerated() {
      text = text.replacingOccurrences(of: unformattedLink, with: replacements[idx])
    }
    return text
  }
  
  func turnEmailIntoFullName() -> String {
    guard let firstPart = self.split(separator: "@").first else { return "" }
    guard let firstName = firstPart.split(separator: ".").first, let lastName = firstPart.split(separator: ".").last else { return "" }
    return "\(firstName.capitalized) \(lastName.capitalized)"
  }
  
  func image(withAttributes attributes: [NSAttributedString.Key: Any]? = nil, size: CGSize? = nil) -> UIImage? {
    let size = size ?? (self as NSString).size(withAttributes: attributes)
    return UIGraphicsImageRenderer(size: size).image { _ in
      (self as NSString).draw(in: CGRect(origin: .zero, size: size),
                              withAttributes: attributes)
    }
  }
  
  static func name<T>(for type: T) -> String {
    String(reflecting: type)
      .components(separatedBy: ".")
      .last ?? ""
  }
}
