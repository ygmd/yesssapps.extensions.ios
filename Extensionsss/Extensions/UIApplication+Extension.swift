//
//  UIApplication+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UIApplication {
  func applicationName() -> String {
    return Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
  }
  
  func applicationVersion() -> String {
    return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
  }
  
  func applicationBuild() -> String {
    return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
  }
  
  func versionBuild() -> String {
    let version = self.applicationVersion()
    let build = self.applicationBuild()
    return "v\(version)(\(build))"
  }
  
  func endEditing() {
    sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
  }
  
  @available(iOS 13.0, *)
  var keyWindow: UIWindow? {
    // Get connected scenes
    return UIApplication.shared.connectedScenes
    // Keep only active scenes, onscreen and visible to the user
      .filter { $0.activationState == .foregroundActive }
    // Keep only the first `UIWindowScene`
      .first(where: { $0 is UIWindowScene })
    // Get its associated windows
      .flatMap({ $0 as? UIWindowScene })?.windows
    // Finally, keep only the key window
      .first(where: \.isKeyWindow)
  }
  
  @available(iOS 13.0, *)
  var keyWindowPresentedController: UIViewController? {
    var viewController = self.keyWindow?.rootViewController
    
    // If root `UIViewController` is a `UITabBarController`
    if let presentedController = viewController as? UITabBarController {
      // Move to selected `UIViewController`
      viewController = presentedController.selectedViewController
    }
    
    // Go deeper to find the last presented `UIViewController`
    while let presentedController = viewController?.presentedViewController {
      // If root `UIViewController` is a `UITabBarController`
      if let presentedController = presentedController as? UITabBarController {
        // Move to selected `UIViewController`
        viewController = presentedController.selectedViewController
      } else {
        // Otherwise, go deeper
        viewController = presentedController
      }
    }
    return viewController
  }
}
