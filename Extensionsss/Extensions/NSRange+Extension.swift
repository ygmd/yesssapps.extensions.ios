//
//  NSRange+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

public extension NSRange {
  func toStringRange( string:String ) -> Range<String.Index>? {
    guard location != NSNotFound else { return nil }
    let start = string.index(string.startIndex, offsetBy: location)
    let end = string.index(string.startIndex, offsetBy: location + length)
    return start..<end
  }
}
