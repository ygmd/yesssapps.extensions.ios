//
//  Int+Extension.swift
//  Extensionsss
//
//  Created by Djamil Secco on 10/01/2019.
//  Copyright © 2019 Yesss Group (W) SA. All rights reserved.
//

import Foundation

public extension Int {
  func stringWithThousandsSeparator() -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.locale = Locale.current
    return formatter.string(from: self as NSNumber) ?? "\(self)"
  }
}
